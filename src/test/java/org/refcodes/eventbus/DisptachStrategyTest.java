// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class DisptachStrategyTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	enum LEVEL {
		_1, _2, _3, _4, _5
	}

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testDispatchStrategies() {
		for ( DispatchStrategy eStrategy : DispatchStrategy.values() ) {
			testDispatchStrategy( eStrategy, 25 );
		}
	}

	@Disabled
	@Test
	public void testDispatchCascade() {
		testDispatchStrategy( DispatchStrategy.CASCADE, 1000 );
	}

	@Disabled
	@Test
	public void testDispatchSequential() {
		testDispatchStrategy( DispatchStrategy.SEQUENTIAL, 1000 );
	}

	@Disabled
	@Test
	public void testDispatchParallel() {
		testDispatchStrategy( DispatchStrategy.PARALLEL, 1000 );
	}

	@Disabled
	@Test
	public void testDispatchAsync() {
		testDispatchStrategy( DispatchStrategy.ASYNC, 1000 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void testDispatchStrategy( DispatchStrategy aDispatchStrategy, int aLoops ) {
		final EventBus theEventBusImpl = new EventBus( aDispatchStrategy );
		final List<EventBusEvent> theEvents = Collections.synchronizedList( new ArrayList<>() );
		theEventBusImpl.onAction( LEVEL._1, ( aEvent ) -> {
			final EventBusEvent theEvent = new EventBusEvent( LEVEL._2, theEventBusImpl );
			synchronized ( theEvents ) {
				theEvents.add( aEvent );
			}
			for ( int i = 0; i < 10; i++ ) {
				theEventBusImpl.publishEvent( theEvent );
			}
		} );
		theEventBusImpl.onAction( LEVEL._2, ( aEvent ) -> {
			final EventBusEvent theEvent = new EventBusEvent( LEVEL._3, theEventBusImpl );
			synchronized ( theEvents ) {
				theEvents.add( aEvent );
			}
			for ( int i = 0; i < 10; i++ ) {
				theEventBusImpl.publishEvent( theEvent );
			}
		} );
		theEventBusImpl.onAction( LEVEL._3, ( aEvent ) -> {
			final EventBusEvent theEvent = new EventBusEvent( LEVEL._4, theEventBusImpl );
			synchronized ( theEvents ) {
				theEvents.add( aEvent );
			}
			for ( int i = 0; i < 10; i++ ) {
				theEventBusImpl.publishEvent( theEvent );
			}
		} );
		theEventBusImpl.onAction( LEVEL._4, ( aEvent ) -> {
			synchronized ( theEvents ) {
				theEvents.add( aEvent );
			}
		} );
		for ( int n = 0; n < aLoops; n++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Iteration <" + n + "> with strategy <" + aDispatchStrategy + "> ..." );
			}
			theEventBusImpl.publishEvent( new EventBusEvent( LEVEL._1, theEventBusImpl ) );
			waitFor( 1111, theEvents );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Events received := " + theEvents.size() );
			}
			theEvents.clear();
		}
	}

	private void waitFor( int aSize, List<EventBusEvent> theEvents ) {
		int theCount = 0;
		do {
			if ( theCount == 10 ) {
				fail( "Did not get expected number of <" + aSize + "> events!" );
			}
			final Predicate<EventBusEvent> predicate1 = e -> e.getAction() == LEVEL._1;
			final Predicate<EventBusEvent> predicate2 = e -> e.getAction() == LEVEL._2;
			final Predicate<EventBusEvent> predicate3 = e -> e.getAction() == LEVEL._3;
			final Predicate<EventBusEvent> predicate4 = e -> e.getAction() == LEVEL._4;
			synchronized ( theEvents ) {
				final long ones = theEvents.stream().filter( predicate1 ).count();
				final long twos = theEvents.stream().filter( predicate2 ).count();
				final long threes = theEvents.stream().filter( predicate3 ).count();
				final long fours = theEvents.stream().filter( predicate4 ).count();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Events received := " + theEvents.size() + ", _1 := " + ones + ", _2 := " + twos + ", _3 := " + threes + ", _4 := " + fours );
				}
			}
			try {
				Thread.sleep( 50 );
			}
			catch ( InterruptedException ignore ) {}
			theCount++;
		} while ( theEvents.size() < aSize );
	}
}
