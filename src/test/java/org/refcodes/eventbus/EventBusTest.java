// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.eventbus.EventBusSugar.*;
import org.junit.jupiter.api.Test;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.mixin.MessageAccessor;
import org.refcodes.observer.Observer;
import org.refcodes.runtime.SystemProperty;

public class EventBusTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	private final EventBus _eventBus = new EventBus();

	@Test
	public void testEventMatcherSchema() {
		final EventBusEventMatcher theSportsMatcher = or( channelEqualWith( "Soccer" ), channelEqualWith( "Football" ) );
		final EventBusEventMatcher thePoliticsMatcher = or( channelEqualWith( "War" ), channelEqualWith( "Education" ) );
		final EventBusEventMatcher theMediaMatcher = or( channelEqualWith( "TV" ), channelEqualWith( "Streaming" ) );
		final EventBusEventMatcher theEntertainmentMatcher = or( theSportsMatcher, theMediaMatcher );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSportsMatcher.toSchema() );
			System.out.println();
			System.out.println( thePoliticsMatcher.toSchema() );
			System.out.println();
			System.out.println( theMediaMatcher.toSchema() );
			System.out.println();
			System.out.println( theEntertainmentMatcher.toSchema() );
			System.out.println();
		}
	}

	@Test
	public void testChannel() {
		final EventBusEventMatcher theSportsMatcher = or( channelEqualWith( "Soccer" ), channelEqualWith( "Football" ) );
		final EventBusEventMatcher thePoliticsMatcher = or( channelEqualWith( "War" ), channelEqualWith( "Education" ) );
		final EventBusObserver theSportsObserver = ( EventBusEvent aEvent ) -> {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Got a sports event on channel <" + aEvent.getMetaData().getChannel() + ">" );
			}
		};
		final EventBusObserver thePoliticsObserver = ( EventBusEvent aEvent ) -> {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Got a politics event on channel <" + aEvent.getMetaData().getChannel() + ">" );
			}
		};
		final EventBus theEventBus = new EventBus();
		theEventBus.subscribe( theSportsMatcher, theSportsObserver );
		theEventBus.subscribe( thePoliticsMatcher, thePoliticsObserver );
		theEventBus.publishEvent( new EventBusEvent( "Soccer", theEventBus ) );
		theEventBus.publishEvent( new EventBusEvent( "Education", theEventBus ) );
		theEventBus.publishEvent( new EventBusEvent( "Football", theEventBus ) );
		theEventBus.publishEvent( new EventBusEvent( "War", theEventBus ) );
		theEventBus.publishEvent( new EventBusEvent( "Formular 1", theEventBus ) );
		theEventBus.publishEvent( new EventBusEvent( "Top Secret", theEventBus ) );
		assertFalse( theEventBus.isMatching( new EventBusEvent( "Top Secret", theEventBus ) ) );
		assertTrue( theEventBus.isMatching( new EventBusEvent( "Soccer", theEventBus ) ) );
		assertTrue( theEventBus.isMatching( new EventBusEvent( "Education", theEventBus ) ) );
	}

	@Test
	public void testEventBus() {
		final EventBusEventMatcher theInitMatcher = and( aliasEqualWith( "EventBusTest" ), channelEqualWith( "org.refcodes.eventbus" ), actionEqualWith( LifecycleStatus.INITIALIZED ) );
		final TestActionObserverImpl theInitObserver = new TestActionObserverImpl( LifecycleStatus.INITIALIZED );
		final String theInitHandle = _eventBus.subscribe( theInitMatcher, theInitObserver );
		final EventBusEventMatcher theStartMatcher = and( aliasEqualWith( "EventBusTest" ), channelEqualWith( "org.refcodes.eventbus" ), actionEqualWith( LifecycleStatus.STARTED ) );
		final TestActionObserverImpl theStartObserver = new TestActionObserverImpl( LifecycleStatus.STARTED );
		final String theStartHandle = _eventBus.subscribe( theStartMatcher, theStartObserver );
		final EventBusEventMatcher theStopMatcher = and( aliasEqualWith( "EventBusTest" ), channelEqualWith( "org.refcodes.eventbus" ), actionEqualWith( LifecycleStatus.STOPPED ) );
		final TestActionObserverImpl theStopObserver = new TestActionObserverImpl( LifecycleStatus.STOPPED );
		final String theStopHandle = _eventBus.subscribe( theStopMatcher, theStopObserver );
		final EventBusEventMatcher theDestroyMatcher = and( aliasEqualWith( "EventBusTest" ), channelEqualWith( "org.refcodes.eventbus" ), actionEqualWith( LifecycleStatus.DESTROYED ) );
		final TestActionObserverImpl theDestroyObserver = new TestActionObserverImpl( LifecycleStatus.DESTROYED );
		final String theDestroyHandle = _eventBus.subscribe( theDestroyMatcher, theDestroyObserver );
		final EventBusEventMatcher theMessageMatcher = and( aliasEqualWith( "EventBusTest" ), channelEqualWith( "org.refcodes.eventbus" ), isAssignableFrom( TestMessageEventImpl.class ) );
		final TestMessageObserver theMessageObserver = new TestMessageObserver();
		final String theMessageHandle = _eventBus.subscribe( theMessageMatcher, theMessageObserver );
		_eventBus.publishEvent( new EventBusEvent( LifecycleStatus.INITIALIZED, "EventBusTest", null, "org.refcodes.eventbus", null, null, _eventBus ) );
		assertEquals( 1, theInitObserver.getEventCount() );
		assertEquals( 0, theStartObserver.getEventCount() );
		assertEquals( 0, theStopObserver.getEventCount() );
		assertEquals( 0, theDestroyObserver.getEventCount() );
		assertEquals( 0, theMessageObserver.getEventCount() );
		_eventBus.publishEvent( new EventBusEvent( LifecycleStatus.STARTED, "EventBusTest", null, "org.refcodes.eventbus", null, null, _eventBus ) );
		assertEquals( 1, theInitObserver.getEventCount() );
		assertEquals( 1, theStartObserver.getEventCount() );
		assertEquals( 0, theStopObserver.getEventCount() );
		assertEquals( 0, theDestroyObserver.getEventCount() );
		assertEquals( 0, theMessageObserver.getEventCount() );
		_eventBus.publishEvent( new EventBusEvent( LifecycleStatus.STOPPED, "EventBusTest", null, "org.refcodes.eventbus", null, null, _eventBus ) );
		assertEquals( 1, theInitObserver.getEventCount() );
		assertEquals( 1, theStartObserver.getEventCount() );
		assertEquals( 1, theStopObserver.getEventCount() );
		assertEquals( 0, theDestroyObserver.getEventCount() );
		assertEquals( 0, theMessageObserver.getEventCount() );
		_eventBus.publishEvent( new EventBusEvent( LifecycleStatus.DESTROYED, "EventBusTest", null, "org.refcodes.eventbus", null, null, _eventBus ) );
		assertEquals( 1, theInitObserver.getEventCount() );
		assertEquals( 1, theStartObserver.getEventCount() );
		assertEquals( 1, theStopObserver.getEventCount() );
		assertEquals( 1, theDestroyObserver.getEventCount() );
		assertEquals( 0, theMessageObserver.getEventCount() );
		_eventBus.publishEvent( new TestMessageEventImpl( "EventBusTest", "org.refcodes.eventbus", "1-2-3 Alert!", _eventBus ) );
		assertEquals( 1, theInitObserver.getEventCount() );
		assertEquals( 1, theStartObserver.getEventCount() );
		assertEquals( 1, theStopObserver.getEventCount() );
		assertEquals( 1, theDestroyObserver.getEventCount() );
		assertEquals( 1, theMessageObserver.getEventCount() );
		final Observer<EventBusEvent> theInitObserver2 = _eventBus.removeHandle( theInitHandle );
		assertEquals( theInitObserver, theInitObserver2 );
		final Observer<EventBusEvent> theStartObserver2 = _eventBus.removeHandle( theStartHandle );
		assertEquals( theStartObserver, theStartObserver2 );
		final Observer<EventBusEvent> theStopObserver2 = _eventBus.removeHandle( theStopHandle );
		assertEquals( theStopObserver, theStopObserver2 );
		final Observer<EventBusEvent> theDestroyObserver2 = _eventBus.removeHandle( theDestroyHandle );
		assertEquals( theDestroyObserver, theDestroyObserver2 );
		final Observer<EventBusEvent> theMessageObserver2 = _eventBus.removeHandle( theMessageHandle );
		assertEquals( theMessageObserver, theMessageObserver2 );
	}

	private int _typeEventCount = 0;

	@Test
	public void testOnType() {
		final EventBus theEventBus = new EventBus();
		theEventBus.onType( TypeEvent.class, this::onTypeEvent );
		theEventBus.publishEvent( new TypeEvent( theEventBus ) );
		assertEquals( 1, _typeEventCount );
	}

	public void onTypeEvent( TypeEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Received an event of type <" + aEvent + "> ..." );
		}
		_typeEventCount++;
	}

	public class TypeEvent extends EventBusEvent {
		public TypeEvent( EventBus aSource ) {
			super( aSource );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class TestActionObserverImpl implements EventBusObserver {
		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final LifecycleStatus _action;
		private int _eventCount = 0;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////
		/**
		 * Instantiates a new test action observer impl.
		 *
		 * @param aAction the action
		 */
		public TestActionObserverImpl( LifecycleStatus aAction ) {
			_action = aAction;
		}
		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		@Override
		public void onEvent( EventBusEvent aEvent ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "LifeCycleStatus = " + aEvent.getAction() + ", Alias = " + aEvent.getMetaData().getAlias() + ", Group = " + aEvent.getMetaData().getGroup() + ", UID = " + aEvent.getMetaData().getUniversalId() + ", publisher type = " + ( aEvent.getMetaData().getPublisherType() != null ? aEvent.getMetaData().getPublisherType().getName() : null ) );
			}
			if ( !_action.equals( aEvent.getAction() ) ) {
				throw new IllegalArgumentException( "Expected an action <" + _action + "> but received the action <" + aEvent.getAction() + ">." );
			}
			_eventCount++;
		}

		public int getEventCount() {
			return _eventCount;
		}
	}

	private class TestMessageObserver implements EventBusObserver {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private int _eventCount = 0;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		public TestMessageObserver() {}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		@Override
		public void onEvent( EventBusEvent aEvent ) {
			final Enum<?> theAction = aEvent.getAction();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "LifeCycleStatus = " + theAction + ", Name = " + aEvent.getMetaData().getAlias() + ", Group = " + aEvent.getMetaData().getGroup() + ", UID = " + aEvent.getMetaData().getUniversalId() + ", publisher type = " + ( aEvent.getMetaData().getPublisherType() != null ? aEvent.getMetaData().getPublisherType().getName() : null ) );
			}
			if ( !( aEvent instanceof TestMessageEventImpl ) ) {
				throw new IllegalArgumentException( "Expected an event of type \"" + TestMessageEventImpl.class.getName() + "\" but received the event \"" + aEvent.getClass().getName() + "\"." );
			}
			_eventCount++;
		}

		public int getEventCount() {
			return _eventCount;
		}
	}

	private class TestMessageEventImpl extends EventBusEvent implements MessageAccessor {

		String _message;

		public TestMessageEventImpl( String aName, String aChannel, String aMessage, EventBus aSource ) {
			super( aName, null, aChannel, null, null, aSource );
			_message = aMessage;
		}

		@Override
		public String getMessage() {
			return _message;
		}
	}
}
