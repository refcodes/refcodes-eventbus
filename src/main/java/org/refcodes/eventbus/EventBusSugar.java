// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus;

import org.refcodes.matcher.Matcher;
import org.refcodes.matcher.MatcherSchema;
import org.refcodes.matcher.MatcherSugar;
import org.refcodes.observer.ActionEqualWithEventMatcher;
import org.refcodes.observer.ActionEvent;
import org.refcodes.observer.AliasEqualWithEventMatcher;
import org.refcodes.observer.CatchAllEventMatcher;
import org.refcodes.observer.CatchNoneEventMatcher;
import org.refcodes.observer.ChannelEqualWithEventMatcher;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.GroupEqualWithEventMatcher;
import org.refcodes.observer.MetaDataEvent;
import org.refcodes.observer.PublisherTypeOfEventMatcher;
import org.refcodes.observer.UniversalIdEqualWithEventMatcher;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the {@link EventBusEventMatcher} elements.
 */
public class EventBusSugar {

	private EventBusSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy} as of
	 * {@link EventBus#EventBus()}.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus eventBus() {
		return new EventBus();
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy} as of
	 * {@link EventBus#EventBus(boolean)}.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus eventBus( boolean isDaemon ) {
		return new EventBus( isDaemon );
	}

	/**
	 * Constructs the {@link EventBus} with the
	 * {@link DispatchStrategy#PARALLEL} when publishing events:
	 * 
	 * Each matching observer is invoked in its own thread. No observer can
	 * block your invoking thread.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus parallelDispatchBus() {
		return new EventBus( DispatchStrategy.PARALLEL );
	}

	/**
	 * Constructs the {@link EventBus} with the
	 * {@link DispatchStrategy#PARALLEL} when publishing events:
	 * 
	 * Each matching observer is invoked in its own thread. No observer can
	 * block your invoking thread.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus parallelDispatchBus( boolean isDaemon ) {
		return new EventBus( DispatchStrategy.PARALLEL, isDaemon );
	}

	/**
	 * Constructs the {@link EventBus} with the
	 * {@link DispatchStrategy#SEQUENTIAL} when publishing events:
	 * 
	 * The parent (invoker) thread is used to publish the parent's event as well
	 * as the child events published by the matching observers of the parent
	 * event (and so on, in case them use {@link DispatchStrategy#SEQUENTIAL} as
	 * well). Any observer (directly or indirectly) invoked by your invoking
	 * thread can block your invoking thread.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus sequentialDispatchBus() {
		return new EventBus( DispatchStrategy.SEQUENTIAL );
	}

	/**
	 * Constructs the {@link EventBus} with the
	 * {@link DispatchStrategy#SEQUENTIAL} when publishing events:
	 * 
	 * The parent (invoker) thread is used to publish the parent's event as well
	 * as the child events published by the matching observers of the parent
	 * event (and so on, in case them use {@link DispatchStrategy#SEQUENTIAL} as
	 * well). Any observer (directly or indirectly) invoked by your invoking
	 * thread can block your invoking thread.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus sequentialDispatchBus( boolean isDaemon ) {
		return new EventBus( DispatchStrategy.SEQUENTIAL, isDaemon );
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy#ASYNC}
	 * when publishing events:
	 * 
	 * Same as the {@link DispatchStrategy#SEQUENTIAL} approach with the
	 * difference that the sequential dispatch process is done asynchronously,
	 * freeing your parent's thread immediately after publishing your parent
	 * event. Exactly one extra thread is created to kick off the asynchronous
	 * way of doing a sequential dispatch. Any observer (directly or indirectly)
	 * invoked by the "asynchronous" thread can block any other observer in that
	 * chain, but not your invoking thread.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus asyncDispatchBus() {
		return new EventBus( DispatchStrategy.ASYNC );
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy#ASYNC}
	 * when publishing events:
	 * 
	 * Same as the {@link DispatchStrategy#SEQUENTIAL} approach with the
	 * difference that the sequential dispatch process is done asynchronously,
	 * freeing your parent's thread immediately after publishing your parent
	 * event. Exactly one extra thread is created to kick off the asynchronous
	 * way of doing a sequential dispatch. Any observer (directly or indirectly)
	 * invoked by the "asynchronous" thread can block any other observer in that
	 * chain, but not your invoking thread.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus asyncDispatchBus( boolean isDaemon ) {
		return new EventBus( DispatchStrategy.ASYNC, isDaemon );
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy#CASCADE}
	 * when publishing events:
	 * 
	 * The parent (invoker) thread is used to publish the parent's event to all
	 * matching observers (and is blocked till done). Child events published by
	 * the matching observers invoked with the parent's event are queued until
	 * the parent's thread finished dispatching the parent's event. The queued
	 * child events then are published in their own separate threads, now
	 * considered being parent events with their according parent threads, to
	 * all matching observers (dispatching as described above). The
	 * {@link DispatchStrategy#CASCADE} strategy is useful when publishing
	 * lifecycle or bootstrapping events to make sure, that any observer was
	 * notified before publishing post-lifecycle actions. Observers directly
	 * invoked by your invoking thread can block your invoking thread and
	 * indirectly invoked observers called by your directly invoked observers
	 * using the {@link DispatchStrategy#SEQUENTIAL} strategy for publishing
	 * their events.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus cascadeDispatchBus() {
		return new EventBus( DispatchStrategy.CASCADE );
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy#CASCADE}
	 * when publishing events:
	 * 
	 * The parent (invoker) thread is used to publish the parent's event to all
	 * matching observers (and is blocked till done). Child events published by
	 * the matching observers invoked with the parent's event are queued until
	 * the parent's thread finished dispatching the parent's event. The queued
	 * child events then are published in their own separate threads, now
	 * considered being parent events with their according parent threads, to
	 * all matching observers (dispatching as described above). The
	 * {@link DispatchStrategy#CASCADE} strategy is useful when publishing
	 * lifecycle or bootstrapping events to make sure, that any observer was
	 * notified before publishing post-lifecycle actions. Observers directly
	 * invoked by your invoking thread can block your invoking thread and
	 * indirectly invoked observers called by your directly invoked observers
	 * using the {@link DispatchStrategy#SEQUENTIAL} strategy for publishing
	 * their events.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link EventBus}.
	 */
	public static EventBus cascadeDispatchBus( boolean isDaemon ) {
		return new EventBus( DispatchStrategy.CASCADE, isDaemon );
	}

	/**
	 * Catches all events, no matching is done.
	 * 
	 * @return The "catch-all" {@link EventBusEventMatcher}.
	 */
	public static EventBusEventMatcher catchAll() {
		return new CatchAllEventBusMatcher();
	}

	/**
	 * Catches no event, no matching is done.
	 * 
	 * @return The "catch-none" {@link EventBusEventMatcher}.
	 */
	public static EventBusEventMatcher catchNone() {
		return new CatchNoneEventBusMatcher();
	}

	/**
	 * Factory method to create an event matcher by event type.
	 * 
	 * @param aEventType The event type to be matched by this matcher.
	 * 
	 * @return An event matcher by event type.
	 */
	public static EventBusEventMatcher isAssignableFrom( Class<?> aEventType ) {
		return new EventBusMatcherWrapper( MatcherSugar.isAssignableFrom( aEventType ) );
	}

	/**
	 * Factory method to create an event matcher by event publisher type.
	 *
	 * @param <PT> The publisher descriptor type
	 * @param aPublisherType The event publisher type to be matched by this
	 *        matcher.
	 * 
	 * @return An event matcher by event type.
	 */
	public static <PT extends Object> EventBusEventMatcher publisherIsAssignableFrom( Class<? extends PT> aPublisherType ) {
		return new PublisherIsAssignableFromEventBusMatcher<PT>( aPublisherType );
	}

	/**
	 * Factory method to create an "OR" matcher for the given matchers.
	 * 
	 * @param aEventMatchers The matchers to be combined by an "OR".
	 * 
	 * @return An "OR" matcher.
	 */
	@SafeVarargs
	public static EventBusEventMatcher or( EventBusEventMatcher... aEventMatchers ) {
		return new EventBusMatcherWrapper( MatcherSugar.or( aEventMatchers ) );
	}

	/**
	 * Factory method to create an "AND" matcher for the given matchers.
	 *
	 * @param aEventMatchers The matchers to be combined by an "AND".
	 * 
	 * @return An "AND" matcher.
	 */
	@SafeVarargs
	public static EventBusEventMatcher and( EventBusEventMatcher... aEventMatchers ) {
		return new EventBusMatcherWrapper( MatcherSugar.and( aEventMatchers ) );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given name
	 * compared with the name stored in the {@link EventMetaData}.
	 *
	 * @param aAlias The name to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s name property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s
	 *         name property.
	 */
	public static EventBusEventMatcher aliasEqualWith( String aAlias ) {
		return new AliasEqualWithEventBusMatcher( aAlias );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given group
	 * compared with the group stored in the {@link EventMetaData}.
	 *
	 * @param aGroup The group to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s group property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s
	 *         group property.
	 */
	public static EventBusEventMatcher groupEqualWith( String aGroup ) {
		return new GroupEqualWithEventBusMatcher( aGroup );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given channel
	 * compared with the channel stored in the {@link EventMetaData}.
	 *
	 * @param aChannel The channel to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s channel property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s
	 *         channel property.
	 */
	public static EventBusEventMatcher channelEqualWith( String aChannel ) {
		return new ChannelEqualWithEventBusMatcher( aChannel );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given UID
	 * compared with the UID stored in the {@link EventMetaData}.
	 *
	 * @param aUid The UID to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s UID property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s UID
	 *         property.
	 */
	public static EventBusEventMatcher uidIdEqualWith( String aUid ) {
		return new UniversalIdEqualWithEventBusMatcher( aUid );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given action
	 * compared with the action stored in the {@link EventMetaData}.
	 *
	 * @param <E> the element type
	 * @param <A> The type of the action stored in the event. CAUTION: The
	 *        drawback of not using generic generic type declaration on a class
	 *        level is no granted type safety, the advantage is the ease of use:
	 *        Sub-classes can be used out of the box.
	 * @param aAction The action to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s action property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link ActionEvent}'s
	 *         action property.
	 */
	public static <E extends EventBusEvent, A> EventBusEventMatcher actionEqualWith( A aAction ) {
		return new ActionEqualWithEventBusMatcher<A>( aAction );
	}

	// INNER CLASSES:

	protected static class CatchAllEventBusMatcher extends CatchAllEventMatcher<EventBusEvent> implements EventBusEventMatcher {}

	protected static class CatchNoneEventBusMatcher extends CatchNoneEventMatcher<EventBusEvent> implements EventBusEventMatcher {}

	private static class PublisherIsAssignableFromEventBusMatcher<PT extends Object> extends PublisherTypeOfEventMatcher<EventBusEvent, PT> implements EventBusEventMatcher {
		public PublisherIsAssignableFromEventBusMatcher( Class<? extends PT> aEventPublisherType ) {
			super( aEventPublisherType );
		}
	}

	private static class AliasEqualWithEventBusMatcher extends AliasEqualWithEventMatcher<EventBusEvent> implements EventBusEventMatcher {
		public AliasEqualWithEventBusMatcher( String aAlias ) {
			super( aAlias );
		}
	}

	private static class GroupEqualWithEventBusMatcher extends GroupEqualWithEventMatcher<EventBusEvent> implements EventBusEventMatcher {
		public GroupEqualWithEventBusMatcher( String aGroup ) {
			super( aGroup );
		}
	}

	private static class ChannelEqualWithEventBusMatcher extends ChannelEqualWithEventMatcher<EventBusEvent> implements EventBusEventMatcher {
		public ChannelEqualWithEventBusMatcher( String aChannel ) {
			super( aChannel );
		}
	}

	private static class UniversalIdEqualWithEventBusMatcher extends UniversalIdEqualWithEventMatcher<EventBusEvent> implements EventBusEventMatcher {
		public UniversalIdEqualWithEventBusMatcher( String aUid ) {
			super( aUid );
		}
	}

	private static class ActionEqualWithEventBusMatcher<A> extends ActionEqualWithEventMatcher<EventBusEvent> implements EventBusEventMatcher {
		public ActionEqualWithEventBusMatcher( A aAction ) {
			super( aAction );
		}
	}

	private static class EventBusMatcherWrapper implements EventBusEventMatcher {

		private final Matcher<EventBusEvent> _eventMatcher;

		public EventBusMatcherWrapper( Matcher<EventBusEvent> aMatcher ) {
			assert ( aMatcher != null );
			_eventMatcher = aMatcher;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isMatching( EventBusEvent aEvent ) {
			return _eventMatcher.isMatching( aEvent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public MatcherSchema toSchema() {
			return new MatcherSchema( _eventMatcher.getClass(), _eventMatcher.toSchema() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getAlias() {
			return _eventMatcher.getAlias();
		}
	}
}
