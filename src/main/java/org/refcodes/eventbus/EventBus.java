// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus;

import static org.refcodes.eventbus.EventBusSugar.*;

import java.util.concurrent.ExecutorService;

import org.refcodes.component.HandleGeneratorImpl;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.MetaDataActionEvent;
import org.refcodes.observer.Observer;

/**
 * The {@link EventBus} defines a predefined {@link EventDispatcher} tailored to
 * use the {@link MetaDataActionEvent} as event type to use. This grants
 * interoperability with other {@link MetaDataActionEvent} based systems.
 */
public class EventBus extends AbstractEventBus<EventBusEvent, Observer<EventBusEvent>, EventBusEventMatcher, EventMetaData, String> implements EventBroker<EventBusEvent, EventBusEventMatcher> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy} being
	 * {@link DispatchStrategy#CASCADE} when publishing events.
	 */
	public EventBus() {
		super( new HandleGeneratorImpl() );
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy} being
	 * {@link DispatchStrategy#CASCADE} when publishing events.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 */
	public EventBus( boolean isDaemon ) {
		super( isDaemon, new HandleGeneratorImpl() );
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy} being
	 * {@link DispatchStrategy#CASCADE} when publishing events.
	 * 
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        creating threads.
	 */
	public EventBus( ExecutorService aExecutorService ) {
		super( new HandleGeneratorImpl(), aExecutorService );
	}

	/**
	 * Constructs the {@link EventBus} with the given {@link DispatchStrategy}
	 * when publishing events.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 */
	public EventBus( DispatchStrategy aDispatchStrategy ) {
		super( aDispatchStrategy, new HandleGeneratorImpl() );
	}

	/**
	 * Constructs the {@link EventBus} with the given {@link DispatchStrategy}
	 * when publishing events.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 */
	public EventBus( DispatchStrategy aDispatchStrategy, boolean isDaemon ) {
		super( aDispatchStrategy, isDaemon, new HandleGeneratorImpl() );
	}

	/**
	 * Constructs the {@link EventBus} with the {@link DispatchStrategy} being
	 * {@link DispatchStrategy#CASCADE} when publishing events.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        creating threads.
	 */
	public EventBus( DispatchStrategy aDispatchStrategy, ExecutorService aExecutorService ) {
		super( aDispatchStrategy, new HandleGeneratorImpl(), aExecutorService );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onAlias( String aName, Observer<EventBusEvent> aObserver ) {
		return subscribe( aliasEqualWith( aName ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onChannel( String aChannel, Observer<EventBusEvent> aObserver ) {
		return subscribe( channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onGroup( String aGroup, Observer<EventBusEvent> aObserver ) {
		return subscribe( channelEqualWith( aGroup ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onUniversalId( String aUid, Observer<EventBusEvent> aObserver ) {
		return subscribe( channelEqualWith( aUid ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onAction( Enum<?> aAction, Observer<EventBusEvent> aObserver ) {
		return subscribe( actionEqualWith( aAction ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onActions( Observer<EventBusEvent> aObserver, Enum<?>... aActions ) {
		final EventBusEventMatcher[] theEqualWith = new EventBusEventMatcher[aActions.length];
		for ( int i = 0; i < theEqualWith.length; i++ ) {
			theEqualWith[i] = actionEqualWith( aActions[i] );
		}
		return subscribe( or( theEqualWith ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onCatchAll( Observer<EventBusEvent> aObserver ) {
		return subscribe( catchAll(), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<?> aPublisherType, Observer<EventBusEvent> aObserver ) {
		return subscribe( publisherIsAssignableFrom( aPublisherType ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( String aChannel, Observer<EventBusEvent> aObserver ) {
		return subscribe( channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), publisherIsAssignableFrom( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Enum<?> aAction, Class<?> aPublisherType, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( actionEqualWith( aAction ), publisherIsAssignableFrom( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Enum<?> aAction, String aChannel, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), publisherIsAssignableFrom( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Enum<?> aAction, Observer<EventBusEvent> aObserver ) {
		return subscribe( actionEqualWith( aAction ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<EventBusEvent> aEventType, Class<?> aPublisherType, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), publisherIsAssignableFrom( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<EventBusEvent> aEventType, String aChannel, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<EventBusEvent> aEventType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), publisherIsAssignableFrom( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<EventBusEvent> aEventType, Enum<?> aAction, Class<?> aPublisherType, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), actionEqualWith( aAction ), publisherIsAssignableFrom( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<EventBusEvent> aEventType, Enum<?> aAction, String aChannel, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<EventBusEvent> aEventType, Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), publisherIsAssignableFrom( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<EventBusEvent> aEventType, Enum<?> aAction, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), actionEqualWith( aAction ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <EVT extends EventBusEvent> String subscribe( Class<EVT> aEventType, EventBusEventMatcher aEventMatcher, Observer<EVT> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), aEventMatcher ), (Observer<EventBusEvent>) aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <EVT extends EventBusEvent> String subscribe( Class<EVT> aEventType, Observer<EVT> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ) ), (Observer<EventBusEvent>) aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <EVT extends EventBusEvent> String onType( Class<EVT> aEventType, Observer<EVT> aObserver ) {
		return subscribe( isAssignableFrom( aEventType ), ( (Observer<EventBusEvent>) aObserver ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onAlias( Class<EventBusEvent> aEventType, String aName, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), aliasEqualWith( aName ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onChannel( Class<EventBusEvent> aEventType, String aChannel, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onGroup( Class<EventBusEvent> aEventType, String aGroup, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), channelEqualWith( aGroup ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onUniversalId( Class<EventBusEvent> aEventType, String aUid, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), channelEqualWith( aUid ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onAction( Class<EventBusEvent> aEventType, Enum<?> aAction, Observer<EventBusEvent> aObserver ) {
		return subscribe( and( isAssignableFrom( aEventType ), actionEqualWith( aAction ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onActions( Class<EventBusEvent> aEventType, Observer<EventBusEvent> aObserver, Enum<?>... aActions ) {
		final EventBusEventMatcher[] theEqualWith = new EventBusEventMatcher[aActions.length];
		for ( int i = 0; i < theEqualWith.length; i++ ) {
			theEqualWith[i] = actionEqualWith( aActions[i] );
		}
		return subscribe( and( isAssignableFrom( aEventType ), or( theEqualWith ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Class<?> aPublisherType ) {
		publishEvent( new EventBusEvent( aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( String aChannel ) {
		publishEvent( new EventBusEvent( aChannel, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new EventBusEvent( aAlias, aGroup, aChannel, aUid, aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, Class<?> aPublisherType ) {
		publishEvent( new EventBusEvent( aAction, aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aChannel ) {
		publishEvent( new EventBusEvent( aAction, aChannel, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aChannel, Class<?> aPublisherType ) {
		publishEvent( new EventBusEvent( aAction, aChannel, aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new EventBusEvent( aAction, aAlias, aGroup, aChannel, aUid, aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( EventMetaData aEventMetaData ) {
		publishEvent( new EventBusEvent( aEventMetaData, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, EventMetaData aEventMetaData ) {
		publishEvent( new EventBusEvent( aAction, aEventMetaData, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction ) {
		publishEvent( new EventBusEvent( aAction, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aPublisherType, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aChannel, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aAlias, aGroup, aChannel, aUid, aPublisherType, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aAction, aPublisherType, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aAction, aChannel, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aAction, aAlias, aGroup, aChannel, aUid, aPublisherType, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aEventMetaData, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aAction, aEventMetaData, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, DispatchStrategy aStrategy ) {
		publishEvent( new EventBusEvent( aAction, this ), aStrategy );
	}
}
