package org.refcodes.eventbus;

import org.refcodes.matcher.Matchable;
import org.refcodes.observer.Event;
import org.refcodes.observer.Publisher;

/**
 * The {@link EventBusPublisher} defines methods to publish events as of
 * {@link #publishEvent(Event)} and to test beforehand if there is a consumer
 * for a given event as of {@link #isMatching(Object)}.
 * 
 * @param <E> the event type being used.
 */
public interface EventBusPublisher<E extends Event<?>> extends Matchable<E>, Publisher<E> {}