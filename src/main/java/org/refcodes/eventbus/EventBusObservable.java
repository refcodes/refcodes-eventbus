package org.refcodes.eventbus;

import org.refcodes.component.HandleLookup;
import org.refcodes.observer.Event;
import org.refcodes.observer.EventMatcher;
import org.refcodes.observer.EventMatcherSugar;
import org.refcodes.observer.MetaDataActionEvent;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observer;

/**
 * The {@link Observable} part of the {@link EventDispatcher} provides means to
 * register {@link Observer} instances.
 * <p>
 * In case your {@link EventBusObservable} notifies upon events of type
 * {@link MetaDataActionEvent}, you may use the {@link MetaDataActionEvent} and
 * an action for distinguishing your various types of notification:
 * <p>
 * TIPP: In order to distinguish {@link MetaDataActionEvent} instances from each
 * other, create an actions enumeration, enumerating the various event actions
 * you support. Pass the actual action you intend to notify upon to the
 * according constructor, as an {@link Observer} you may use the declarative
 * method {@link EventMatcherSugar#actionEqualWith(Object)} to test whether your
 * action was notified (or a simple switch case statement).
 *
 * @param <E> The {@link Event} type to be used.
 * @param <O> The source to by used.
 * @param <MATCH> The event matcher to be used.
 * @param <H> The handle to be used.
 */
public interface EventBusObservable<E extends Event<?>, O extends Observer<E>, MATCH extends EventMatcher<E>, H> extends HandleLookup<H, O> {

	/**
	 * Subscribes a listener to the event bus. In case the handle is being
	 * ignored, use the "unsubscribeAll" method where all subscriptions for a
	 * listener are removed.
	 * 
	 * @param aEventMatcher The {@link EventMatcher} to guard the
	 *        {@link Observer}.
	 * @param aObserver The {@link Observer} to subscribe.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	H subscribe( MATCH aEventMatcher, O aObserver );

	/**
	 * Subscribes a listener to the event bus. In case the handle is being
	 * ignored, use the "unsubscribeAll" method where all subscriptions for a
	 * listener are removed.
	 * 
	 * @param <EVT> The type of the event to subscribe for.
	 * @param aEventType The type of event to observe.
	 * @param aObserver The {@link Observer} to subscribe.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	<EVT extends E> H subscribe( Class<EVT> aEventType, Observer<EVT> aObserver );

	/**
	 * Subscribes a listener to the event bus. In case the handle is being
	 * ignored, use the "unsubscribeAll" method where all subscriptions for a
	 * listener are removed.
	 * 
	 * @param <EVT> The type of the event to subscribe for.
	 * @param aEventType The type of event to observe.
	 * @param aEventMatcher The {@link EventMatcher} to guard the
	 *        {@link Observer}.
	 * @param aObserver The {@link Observer} to subscribe.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	<EVT extends E> H subscribe( Class<EVT> aEventType, MATCH aEventMatcher, Observer<EVT> aObserver );

	/**
	 * Unsubscribes all registrations to a specific listener, even if that
	 * listener is involved in several subscriptions.
	 *
	 * @param aObserver the observer
	 */
	void unsubscribeAll( O aObserver );

	/**
	 * Unsubscribes the listener associated with the given handle. <T> As of
	 * convenience, the type of the reference returned. CAUTION: The type <T>
	 * being a sub-type of <OD> has the drawback that in case you specify a
	 * sub-type of <OD> (<T>), you may end up with a class cast exception in
	 * case you do not make sure that the handle references the expected type
	 * <T>.
	 * 
	 * @param aHandle The handle in question.
	 * 
	 * @return The event listener descriptor used to describe the listener and
	 *         the provided criteria.
	 * 
	 * @throws NoSuchHandleRuntimeException in case the handle is unknown by the
	 *         event-bus.
	 */
	// T unsubscribe( H aHandle ) throws UnknownHandleRuntimeException;
}