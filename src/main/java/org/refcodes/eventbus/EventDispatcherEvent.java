// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus;

import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.MetaDataActionEvent;

/**
 * Intuitive Meta-Interface for the {@link MetaDataActionEvent} as used by the
 * {@link EventBus}.
 * 
 * @param <A> The type of the action stored in the event.
 * @param <META> The type of the EventMetaData
 * @param <SRC> The type of the source in question.
 */
public interface EventDispatcherEvent<A, META extends EventMetaData, SRC> extends MetaDataActionEvent<A, META, SRC> {

	/**
	 * Intuitive Meta-Interface for the {@link EventDispatcherEventBuilder} as
	 * used by the {@link EventDispatcher}.
	 * 
	 * @param <A> The type of the action stored in the event.
	 * @param <META> The type of the EventMetaData
	 * @param <SRC> The type of the source in question.
	 */
	public interface EventDispatcherEventBuilder<A, META extends EventMetaData, SRC extends EventDispatcher<?, ?, ?, ?, ?>, B extends EventDispatcherEventBuilder<A, META, SRC, B>> extends MetaDataBuilder<META, B>, EventDispatcherEvent<A, META, SRC> {}
}
