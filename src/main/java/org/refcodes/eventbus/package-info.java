// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifacts implements a lightweight infrastructure for event based
 * decoupled communication. This is achieved by implementing the
 * <a href="http://en.wikipedia.org/wiki/Observer_pattern">Observer pattern</a>
 * combined with the <a href=
 * "http://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern">Publish–subscribe
 * pattern</a>, ending up being a
 * <a href="http://en.wikipedia.org/wiki/Message_broker">Message broker</a> for
 * <code>events</code>.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-eventbus"><strong>refcodes-eventbus:
 * Observer + Publish/Subscribe = Message broker</strong></a> documentation for
 * an up-to-date and detailed description on the usage of this artifact.
 * </p>
 */
package org.refcodes.eventbus;
