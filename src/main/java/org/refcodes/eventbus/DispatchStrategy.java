// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus;

/**
 * The {@link DispatchStrategy} defines how child events are published within a
 * parent event publish cycle.
 */
public enum DispatchStrategy {

	/**
	 * The parent (invoker) thread is used to publish the parent's event as well
	 * as the child events published by the matching observers of the parent
	 * event (and so on, in case them use {@link #SEQUENTIAL} as well). Any
	 * observer (directly or indirectly) invoked by your invoking thread can
	 * block your invoking thread.
	 */
	SEQUENTIAL,

	/**
	 * Same as the {@link #SEQUENTIAL} approach with the difference that the
	 * sequential dispatch process is done asynchronously, freeing your parent's
	 * thread immediately after publishing your parent event. Exactly one extra
	 * thread is created to kick off the asynchronous way of doing a sequential
	 * dispatch. Any observer (directly or indirectly) invoked by the
	 * "asynchronous" thread can block any other observer in that chain, but not
	 * your invoking thread.
	 */
	ASYNC,

	/**
	 * The parent (invoker) thread is used to publish the parent's event to all
	 * matching observers (and is blocked till done). Child events published by
	 * the matching observers invoked with the parent's event are queued until
	 * the parent's thread finished dispatching the parent's event. The queued
	 * child events then are published in their own separate threads, now
	 * considered being parent events with their according parent threads, to
	 * all matching observers (dispatching as described above). The
	 * {@link #CASCADE} strategy is useful when publishing lifecycle or
	 * bootstrapping events to make sure, that any observer was notified before
	 * publishing post-lifecycle actions. Observers directly invoked by your
	 * invoking thread can block your invoking thread and indirectly invoked
	 * observers called by your directly invoked observers using the
	 * {@link DispatchStrategy#SEQUENTIAL} strategy for publishing their events.
	 */
	CASCADE,

	/**
	 * Each matching observer is invoked in its own thread. No observer can
	 * block your invoking thread.
	 */
	PARALLEL

}
