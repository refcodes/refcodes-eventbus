package org.refcodes.eventbus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

import javax.security.auth.Destroyable;

import org.refcodes.component.HandleGenerator;
import org.refcodes.component.UnknownHandleRuntimeException;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observer;
import org.refcodes.observer.ObserverDescriptor;

/**
 * The Class AbstractEventBus.
 *
 * @param <EVT> the element type
 * @param <O> the generic type
 * @param <MATCH> the generic type
 * @param <META> the generic type
 * @param <H> the generic type
 */
public abstract class AbstractEventBus<EVT extends EventBrokerEvent<?>, O extends Observer<EVT>, MATCH extends EventBrokerEventMatcher<EVT>, META extends EventMetaData, H> implements EventDispatcher<EVT, O, MATCH, META, H>, Destroyable {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean DAEMON_BY_DEFAULT = true;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Map<H, ObserverDescriptor<EVT, O, MATCH>> _handleToObserverDescriptors = new HashMap<>();
	private final HandleGenerator<H> _handleGenerator;
	private DispatchStrategy _dispatchStrategy;
	private final ExecutorService _executorService;
	private Map<Thread, List<EVT>> _threadQueue;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AbstractEventBus} with the {@link DispatchStrategy}
	 * being {@link DispatchStrategy#CASCADE} when publishing events and the
	 * given handle generator.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @param aHandleGenerator The handle generator to be used.
	 */
	public AbstractEventBus( boolean isDaemon, HandleGenerator<H> aHandleGenerator ) {
		assert ( aHandleGenerator != null );
		_handleGenerator = aHandleGenerator;
		_executorService = ControlFlowUtility.createCachedExecutorService( isDaemon );
		initDispatchStrategy( DispatchStrategy.CASCADE );
	}

	/**
	 * Constructs the {@link AbstractEventBus} with the {@link DispatchStrategy}
	 * being {@link DispatchStrategy#CASCADE} when publishing events and the
	 * given handle generator, any {@link Thread} instances created are created
	 * as daemon threads.
	 * 
	 * @param aHandleGenerator The handle generator to be used.
	 */
	public AbstractEventBus( HandleGenerator<H> aHandleGenerator ) {
		assert ( aHandleGenerator != null );
		_handleGenerator = aHandleGenerator;
		_executorService = ControlFlowUtility.createCachedExecutorService( DAEMON_BY_DEFAULT );
		initDispatchStrategy( DispatchStrategy.CASCADE );
	}

	/**
	 * Constructs the {@link AbstractEventBus} with the {@link DispatchStrategy}
	 * being {@link DispatchStrategy#CASCADE} when publishing events and the
	 * given handle generator.
	 * 
	 * @param aHandleGenerator The handle generator to be used.
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        creating threads.
	 */
	public AbstractEventBus( HandleGenerator<H> aHandleGenerator, ExecutorService aExecutorService ) {
		assert ( aHandleGenerator != null );
		_handleGenerator = aHandleGenerator;
		_executorService = aExecutorService != null ? aExecutorService : ControlFlowUtility.createCachedExecutorService( true );
		initDispatchStrategy( DispatchStrategy.CASCADE );
	}

	/**
	 * Constructs the {@link AbstractEventBus} with the given
	 * {@link DispatchStrategy} when publishing events and the given handle
	 * generator, any {@link Thread} instances created are created as daemon
	 * threads.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 * @param aHandleGenerator The handle generator to be used.
	 */
	public AbstractEventBus( DispatchStrategy aDispatchStrategy, HandleGenerator<H> aHandleGenerator ) {
		assert ( aHandleGenerator != null );
		assert ( aDispatchStrategy != null );
		_handleGenerator = aHandleGenerator;
		_executorService = ControlFlowUtility.createCachedExecutorService( DAEMON_BY_DEFAULT );
		initDispatchStrategy( aDispatchStrategy );
	}

	/**
	 * Constructs the {@link AbstractEventBus} with the given
	 * {@link DispatchStrategy} when publishing events and the given handle
	 * generator.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @param aHandleGenerator The handle generator to be used.
	 */
	public AbstractEventBus( DispatchStrategy aDispatchStrategy, boolean isDaemon, HandleGenerator<H> aHandleGenerator ) {
		assert ( aHandleGenerator != null );
		assert ( aDispatchStrategy != null );
		_handleGenerator = aHandleGenerator;
		_executorService = ControlFlowUtility.createCachedExecutorService( isDaemon );
		initDispatchStrategy( aDispatchStrategy );
	}

	/**
	 * Constructs the {@link AbstractEventBus} with the given
	 * {@link DispatchStrategy} when publishing events and the given handle
	 * generator.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 * @param aHandleGenerator The handle generator to be used.
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        creating threads.
	 */
	public AbstractEventBus( DispatchStrategy aDispatchStrategy, HandleGenerator<H> aHandleGenerator, ExecutorService aExecutorService ) {
		assert ( aHandleGenerator != null );
		assert ( aDispatchStrategy != null );
		_handleGenerator = aHandleGenerator;
		_executorService = aExecutorService != null ? aExecutorService : ControlFlowUtility.createCachedExecutorService( true );
		initDispatchStrategy( aDispatchStrategy );
	}

	/**
	 * Initializes the data structures required by the according
	 * {@link DispatchStrategy}.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} for which to the
	 *        required data structures.
	 */
	private void initDispatchStrategy( DispatchStrategy aDispatchStrategy ) {
		_dispatchStrategy = aDispatchStrategy;
		_threadQueue = new WeakHashMap<>();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( EVT aEvent ) {
		final List<ObserverDescriptor<EVT, O, MATCH>> theListeners;
		synchronized ( _handleToObserverDescriptors ) {
			theListeners = new ArrayList<>( _handleToObserverDescriptors.values() );
		}
		for ( ObserverDescriptor<EVT, O, MATCH> eEventListenerDescriptor : theListeners ) {
			if ( eEventListenerDescriptor.getEventMatcher().isMatching( aEvent ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public H subscribe( MATCH aEventMatcher, O aObserver ) {
		final H theHandle = _handleGenerator.next();
		synchronized ( _handleToObserverDescriptors ) {
			_handleToObserverDescriptors.put( theHandle, new ObserverDescriptor<>( aObserver, aEventMatcher ) );
		}
		return theHandle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void unsubscribeAll( O aObserver ) {
		// Using an iterator to safely remove elements while iterating:
		final List<ObserverDescriptor<EVT, O, MATCH>> theListeners;
		synchronized ( _handleToObserverDescriptors ) {
			theListeners = new ArrayList<>( _handleToObserverDescriptors.values() );
		}
		final Iterator<ObserverDescriptor<EVT, O, MATCH>> e = theListeners.iterator();
		ObserverDescriptor<EVT, O, MATCH> eReference;
		while ( e.hasNext() ) {
			eReference = e.next();
			// Remove by identity!
			if ( eReference.getObserver() == aObserver ) {
				e.remove();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasHandle( H aHandle ) {
		synchronized ( _handleToObserverDescriptors ) {
			return _handleToObserverDescriptors.containsKey( aHandle );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public O lookupHandle( H aHandle ) {
		final ObserverDescriptor<EVT, O, MATCH> theReference;
		synchronized ( _handleToObserverDescriptors ) {
			theReference = _handleToObserverDescriptors.get( aHandle );
		}
		if ( theReference == null ) {
			throw new UnknownHandleRuntimeException( aHandle, "The given handle is not known by this system." );
		}
		return theReference.getObserver();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public O removeHandle( H aHandle ) {
		final ObserverDescriptor<EVT, O, MATCH> theReference;
		synchronized ( _handleToObserverDescriptors ) {
			theReference = _handleToObserverDescriptors.remove( aHandle );
		}
		if ( theReference == null ) {
			throw new UnknownHandleRuntimeException( aHandle, "The given handle is not known by this system." );
		}
		return theReference.getObserver();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DispatchStrategy getDispatchStrategy() {
		return _dispatchStrategy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( EVT aEvent, DispatchStrategy aDispatchStrategy ) {
		switch ( aDispatchStrategy ) {
		case CASCADE -> cascadeDispatch( aEvent, new Object() );
		case SEQUENTIAL -> sequentialDispatch( aEvent );
		case PARALLEL -> parallelDispatch( aEvent );
		case ASYNC -> _executorService.execute( () -> sequentialDispatch( aEvent ) );
		}
	}

	/**
	 * Shuts down any {@link ExecutorService} by calling
	 * {@link ExecutorService#shutdown()} {@inheritDoc} causing the
	 * {@link EventDispatcher} to be disposed.
	 */
	@Override
	public void destroy() {
		_executorService.shutdown();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook implementing the sequential dispatch method as of
	 * {@link DispatchStrategy#SEQUENTIAL}.
	 *
	 * @param aEvent The event to be dispatched sequentially.
	 */
	protected void sequentialDispatch( EVT aEvent ) {
		final List<ObserverDescriptor<EVT, O, MATCH>> theListeners;
		synchronized ( _handleToObserverDescriptors ) {
			theListeners = new ArrayList<>( _handleToObserverDescriptors.values() );
		}
		for ( ObserverDescriptor<EVT, O, MATCH> eListener : theListeners ) {
			eListener.onEvent( aEvent );
		}
	}

	/**
	 * Hook implementing the parallel dispatch method as of
	 * {@link DispatchStrategy#PARALLEL}.
	 *
	 * @param aEvent The event to be dispatched sequentially.
	 */
	protected void parallelDispatch( EVT aEvent ) {
		final List<ObserverDescriptor<EVT, O, MATCH>> theListeners;
		synchronized ( _handleToObserverDescriptors ) {
			theListeners = new ArrayList<>( _handleToObserverDescriptors.values() );
		}
		for ( ObserverDescriptor<EVT, O, MATCH> eListener : theListeners ) {
			if ( eListener.getEventMatcher().isMatching( aEvent ) ) {
				_executorService.execute( () -> eListener.getObserver().onEvent( aEvent ) );
			}
		}
	}

	/**
	 * Hook implementing the cascaded dispatch method as of
	 * {@link DispatchStrategy#CASCADE}.
	 *
	 * @param aEvent The event to be dispatched cascading.
	 * @param aMonitor The monitor used for synchronizing this cascade.
	 */
	protected void cascadeDispatch( EVT aEvent, Object aMonitor ) {
		List<EVT> theQueue;
		theQueue = _threadQueue.get( Thread.currentThread() );
		if ( theQueue == null ) {
			theQueue = new ArrayList<>();
			synchronized ( aMonitor ) {
				try {
					_threadQueue.put( Thread.currentThread(), theQueue );
					final List<ObserverDescriptor<EVT, O, MATCH>> theListeners;
					synchronized ( _handleToObserverDescriptors ) {
						theListeners = new ArrayList<>( _handleToObserverDescriptors.values() );
					}
					for ( ObserverDescriptor<EVT, O, MATCH> eListener : theListeners ) {
						eListener.onEvent( aEvent );
					}
					for ( EVT eEvent : theQueue ) {
						_executorService.execute( () -> cascadeDispatch( eEvent, aMonitor ) );
					}
				}
				finally {
					theQueue.clear();
					_threadQueue.remove( Thread.currentThread() );
				}
			}
		}
		else {
			theQueue.add( aEvent );
		}
	}
}