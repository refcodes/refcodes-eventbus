package org.refcodes.eventbus;

import org.refcodes.component.HandleGenerator;
import org.refcodes.observer.ActionEvent;
import org.refcodes.observer.Event;
import org.refcodes.observer.EventMatcher;
import org.refcodes.observer.EventMatcherSugar;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.MetaDataActionEvent;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observer;

/**
 * The {@link EventDispatcher} is the (virtual) machine wide manager for
 * {@link ActionEvent} handling.
 * 
 * TIPP: In order to distinguish {@link MetaDataActionEvent} instances from each
 * other, create an actions enumeration, enumerating the various event actions
 * you support. Pass the actual action you intend to notify upon to the
 * according constructor, as an {@link Observer} you may use the declarative
 * method {@link EventMatcherSugar#actionEqualWith(Object)} to test whether your
 * action was notified (or a simple switch case statement).
 *
 * @param <E> the {@link Event} type.
 * @param <O> The event's source
 * @param <MATCH> the {@link EventMatcher} type.
 * @param <META> the {@link EventMetaData} type.
 * @param <H> the {@link HandleGenerator} type.
 */
public interface EventDispatcher<E extends Event<?>, O extends Observer<E>, MATCH extends EventMatcher<E>, META extends EventMetaData, H> extends EventBusPublisher<E>, EventBusObservable<E, O, MATCH, H>, DispatchStrategyAccessor {

	/**
	 * Publishes an event using the {@link DispatchStrategy} as returned by
	 * {@link #getDispatchStrategy()}. {@inheritDoc}
	 */
	@Override
	default void publishEvent( E aEvent ) {
		publishEvent( aEvent, getDispatchStrategy() );
	}

	/**
	 * Publishes an event using the given {@link DispatchStrategy}.
	 * 
	 * @param aEvent aEvent The event to be published.
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing the event.
	 */
	void publishEvent( E aEvent, DispatchStrategy aDispatchStrategy );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes.
	 *
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Class<?> aPublisherType, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( String aChannel, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes.
	 *
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes.
	 *
	 * @param aAction The action which this represents.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Enum<?> aAction, Class<?> aPublisherType, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes.
	 *
	 * @param aAction The action which this represents.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Enum<?> aAction, String aChannel, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes.
	 *
	 * @param aAction The action which this represents.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes. Your {@link Observable} may be of the required
	 * type!
	 *
	 * @param aAction The action which this represents.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Enum<?> aAction, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given name.
	 * 
	 * @param aAlias The name for which to subscribe.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onAlias( String aAlias, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given channel.
	 * 
	 * @param aChannel The channel to which to subscribe to.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onChannel( String aChannel, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given group.
	 * 
	 * @param aGroup The group to which to subscribe to.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onGroup( String aGroup, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given Universal-TID.
	 * 
	 * @param aUid The Universal-TID to which to subscribe to.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onUniversalId( String aUid, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given action.
	 * 
	 * @param aAction The action to which to subscribe to.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onAction( Enum<?> aAction, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given action.
	 * 
	 * @param aObserver The observer to be notified.
	 * @param aActions The actions to which to subscribe to.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onActions( O aObserver, Enum<?>... aActions );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes all events
	 * passing the Event-Bus. Call it a "catch-all" hook.
	 * 
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onCatchAll( O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes. Your {@link Observable} may be of the required
	 * type!
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Class<E> aEventType, Class<?> aPublisherType, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes. Your {@link Observable} may be of the required
	 * type!
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Class<E> aEventType, String aChannel, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes. Your {@link Observable} may be of the required
	 * type!
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Class<E> aEventType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes. Your {@link Observable} may be of the required
	 * type!
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aAction The action which this represents.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Class<E> aEventType, Enum<?> aAction, Class<?> aPublisherType, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes. Your {@link Observable} may be of the required
	 * type!
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aAction The action which this represents.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Class<E> aEventType, Enum<?> aAction, String aChannel, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes. Your {@link Observable} may be of the required
	 * type!
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aAction The action which this represents.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Class<E> aEventType, Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given attributes. Your {@link Observable} may be of the required
	 * type!
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aAction The action which this represents.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onEvent( Class<E> aEventType, Enum<?> aAction, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * of the given type. Your {@link Observable} may be of the required type!
	 * 
	 * @param <EVT> The type of the event to subscribe for.
	 * @param aEventType The event type to subscribe for.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	<EVT extends E> String onType( Class<EVT> aEventType, Observer<EVT> aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * with the given name.
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aName The name for which to subscribe.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onAlias( Class<E> aEventType, String aName, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given channel.
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aChannel The channel to which to subscribe to.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onChannel( Class<E> aEventType, String aChannel, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given group.
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aGroup The group to which to subscribe to.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onGroup( Class<E> aEventType, String aGroup, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given Universal-TID.
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aUid The Universal-TID to which to subscribe to.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onUniversalId( Class<E> aEventType, String aUid, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given action.
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aAction The action to which to subscribe to.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onAction( Class<E> aEventType, Enum<?> aAction, O aObserver );

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for events
	 * for the given action.
	 *
	 * @param aEventType The event type to subscribe for.
	 * @param aObserver The observer to be notified.
	 * @param aActions The actions to which to subscribe to.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	String onActions( Class<E> aEventType, O aObserver, Enum<?>... aActions );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 */
	void publishEvent( Class<?> aPublisherType );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 */
	void publishEvent( String aChannel );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	void publishEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aPublisherType The type of the event publisher.
	 */
	void publishEvent( Enum<?> aAction, Class<?> aPublisherType );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	void publishEvent( Enum<?> aAction, String aChannel );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 */
	void publishEvent( Enum<?> aAction, String aChannel, Class<?> aPublisherType );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	void publishEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aEventMetaData The Meta-Data to be supplied by the event.
	 */
	void publishEvent( META aEventMetaData );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aEventMetaData The Meta-Data to be supplied by the event.
	 */
	void publishEvent( Enum<?> aAction, META aEventMetaData );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 */
	void publishEvent( Enum<?> aAction );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( Class<?> aPublisherType, DispatchStrategy aStrategy );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( String aChannel, DispatchStrategy aStrategy );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( Enum<?> aAction, Class<?> aPublisherType, DispatchStrategy aStrategy );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( Enum<?> aAction, String aChannel, DispatchStrategy aStrategy );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aEventMetaData The Meta-Data to be supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( META aEventMetaData, DispatchStrategy aStrategy );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aEventMetaData The Meta-Data to be supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( Enum<?> aAction, META aEventMetaData, DispatchStrategy aStrategy );

	/**
	 * Fires an event with the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	void publishEvent( Enum<?> aAction, DispatchStrategy aStrategy );

}