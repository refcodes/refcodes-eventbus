// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus;

/**
 * Provides an accessor for a {@link DispatchStrategy} property.
 */
public interface DispatchStrategyAccessor {

	/**
	 * Retrieves the {@link DispatchStrategy} property from the property.
	 * Determines in which {@link DispatchStrategy} status a component is in.
	 * 
	 * @return Returns the {@link DispatchStrategy} property stored by the
	 *         property.
	 */
	DispatchStrategy getDispatchStrategy();

	/**
	 * Provides a mutator for a {@link DispatchStrategy} property.
	 */
	public interface DispatchStrategyMutator {

		/**
		 * Sets the {@link DispatchStrategy} property for the property.
		 * 
		 * @param aLifeCycle The {@link DispatchStrategy} property to be stored
		 *        by the property.
		 */
		void setDispatchStrategy( DispatchStrategy aLifeCycle );
	}

	/**
	 * Provides a {@link DispatchStrategy} property.
	 */
	public interface DispatchStrategyProperty extends DispatchStrategyAccessor, DispatchStrategyMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link DispatchStrategy}
		 * (setter) as of {@link #setDispatchStrategy(DispatchStrategy)} and
		 * returns the very same value (getter).
		 * 
		 * @param aDispatchStrategy The {@link DispatchStrategy} to set (via
		 *        {@link #setDispatchStrategy(DispatchStrategy)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default DispatchStrategy letDispatchStrategy( DispatchStrategy aDispatchStrategy ) {
			setDispatchStrategy( aDispatchStrategy );
			return aDispatchStrategy;
		}
	}
}
