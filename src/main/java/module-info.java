module org.refcodes.eventbus {
	requires org.refcodes.controlflow;
	requires org.refcodes.matcher;
	requires transitive org.refcodes.schema;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.observer;

	exports org.refcodes.eventbus;
}
